import socket

class videosocket:
    def __init__(self , sock=None):
        if sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock= sock

    def connect(self,host,port):
        self.sock.connect((host,port))

    def vsend(self, framestring):
        totalsent = 0
        length =len(framestring)
        print('length',length)
        metasent = 0
        lengthstr=str(length).zfill(8)

        while metasent < 8 :
            sent = self.sock.send(lengthstr[metasent:].encode())
            print(sent)
            metasent += sent

        print('8',length,totalsent)
        while totalsent < length :
            sent = self.sock.send(framestring[totalsent:])
            totalsent += sent

    def vreceive(self):
        metaArray = []
        totrec=0
        msgArray = []
        metarec=0
        while metarec < 8:
            #print(chunk)
            chunk = self.sock.recv(8 - metarec)
            #print(chunk)
            metaArray.append(chunk)
            #print(metaArray)
            metarec += len(chunk)
        #print(type(metaArray))
        #lengthstr=metaArray[0]
        lengthstr= b''.join(metaArray)
        print(lengthstr)
        length=int(lengthstr)

        while totrec<length :
            chunk = self.sock.recv(length - totrec)
            #print(chunk)
            if chunk == '':
                raise RuntimeError("Socket connection broken")
            msgArray.append(chunk)
            totrec += len(chunk)
        #print(msgArray)
        return b''.join(msgArray)
