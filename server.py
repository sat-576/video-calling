import socket, vsocket
from vfeed import VideoFeed

class Server:
    def __init__(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(("", 12350))
        self.server_socket.listen(5)
        self.vfeed = VideoFeed(1,"server",1)
        print ("TCPServer Waiting for client on port 12350")

    def start(self):
        while 1:
            client_socket, address = self.server_socket.accept()
            print ("I got a connection from ", address)
            vsock = videosocket.videosocket(client_socket)
            while True:
                frame=vsock.vreceive()
                self.vfeed.set_frame(frame)
                frame=self.vfeed.get_frame()
                vsock.vsend(frame)

if __name__ == "__main__":
    server = Server()
    server.start()
